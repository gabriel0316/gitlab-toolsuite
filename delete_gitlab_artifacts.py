import requests
import argparse

parser = argparse.ArgumentParser(usage="%(prog)s [-p projectid] [-t privatetoken]")
parser.add_argument("-p", "--projectid", help='GitLab project ID', required=True)
parser.add_argument("-t", "--privatetoken", help='GitLab private token', required=True)
args = parser.parse_args()

project_id = args.projectid
base_url = 'https://gitlab.com/api/v4/projects/' + project_id + '/jobs'
headers = {'PRIVATE-TOKEN':args.privatetoken}
params = {'per_page':'100'}

def perform_request():
  r = requests.get(url=base_url,headers=headers,params=params)
  data = r.json()

  return data, r.headers

last_round = False

while not last_round:
  data, resp_headers = perform_request()
  count = len(data)
  print("number of artifacts:" + str(count))
  print("current page: " + resp_headers['X-Page'])

  for i,job in enumerate(data):
    print('---'+str(i+1)+'/'+str(count)+'---')
    print('deleting artifacts of job ' + str(job['id']))
    response = requests.delete(url=base_url + '/' + str(job['id']) + '/artifacts', headers=headers)
    print(response)

  print("next page: " + resp_headers['X-Next-Page'])
  params['page'] = resp_headers['X-Next-Page']
  last_round = resp_headers['X-Next-Page'] == ''
  print("was this the last page: " + str(last_round))
  print("-----------------------")


